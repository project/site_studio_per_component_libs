Site Studio Per Component Library

Installation
============

- Enable Site Studio Per Component Library module in the Drupal administration.


Usage
============

This module checks for site studio layout canvas fields in nodes and automatically attach libraries for the current theme based on the component machine name.

All you need to do is to define your libraries on your custom theme as so:

cpt_my_component_machine_name:
  css:
    component:
      css/cpt_my_component_machine_name.css: {}
  js:
    js/cpt_my_component_machine_name.js: {}

cpt_my_component_machine_name2:
  css:
    component:
      css/cpt_my_component_machine_name2.css: {}

